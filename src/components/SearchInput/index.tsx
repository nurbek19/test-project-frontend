import React from "react";
import './index.css';

type PropTypes = {
  changeHandler: (event: React.ChangeEvent<HTMLInputElement>) => void
}

const SearchInput: React.FC<PropTypes> = (props) => {
  return (
    <input
      className="search-input"
      type="text"
      onChange={props.changeHandler}
      placeholder="Введите исполнителя"
    />
  )
};

export default SearchInput;

