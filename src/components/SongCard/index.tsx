import React from "react";

import {Song} from "../../store/types";
import {convertToMinute} from "../../containers/Songs";

import './index.css';

type PropTypes = {
  song: Song
}

const SongCard: React.FC<PropTypes> = ({song}) => {
  return (
    <div className="song-card">
      <a href={song.link}>
        <div className="about-song">
          <img src={song.albumCover} alt="album cover" />

          <div>
            <p className="song-title">{song.name}</p>
            <p className="song-artist">
              <span>Исполнители:</span>
              {song.artist}
            </p>
          </div>
        </div>

        <div className="song-duration">{convertToMinute(song.duration)}</div>
      </a>
    </div>
  )
};

export default SongCard;
