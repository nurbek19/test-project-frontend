import React from "react";

import searchIcon from '../../assets/icons/search-icon.svg';
import loadingIcon from '../../assets/icons/loading-icon.svg';
import './index.css';

type PropTypes = {
  disabled: boolean;
  loading: boolean;
  clickHandler: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const SearchButton: React.FC<PropTypes> = (props) => {
  return (
    <button className="search-button" disabled={props.disabled} onClick={props.clickHandler}>
      <img src={props.loading ? loadingIcon : searchIcon} alt="search icon" />
    </button>
  )
};

export default SearchButton;
