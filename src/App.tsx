import React from 'react';
import SearchBar from "./containers/SearchBar";
import Songs from "./containers/Songs";

function App() {
  return (
    <div className="App">
      <SearchBar />
      <Songs />
    </div>
  );
}

export default App;
