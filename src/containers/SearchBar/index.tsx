import React, {Component} from "react";
import {AnyAction} from "redux";
import {connect, ConnectedProps} from "react-redux";
import {ThunkDispatch} from "redux-thunk";

import SearchInput from "../../components/SearchInput";
import SearchButton from "../../components/SearchButton";

import {RootState} from "../../store/configureStore";
import {fetchSongs} from "../../store/actions";

import './index.css';


const mapStateToProps = (state: RootState) => ({
  loading: state.loading
});

const mapDispatchToProps = (dispatch: ThunkDispatch<RootState, void, AnyAction>) => ({
  fetchSongs: (singer: string) => dispatch(fetchSongs(singer))
});

const connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

type PropsFromRedux = ConnectedProps<typeof connector>

type SearchState = {
  singer: string
}

class SearchBar extends Component<PropsFromRedux, SearchState> {
  state: SearchState = {
    singer: ''
  };

  changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({singer: event.target.value})
  };

  clickHandler = (): void => {
    this.props.fetchSongs(this.state.singer);
  };

  render() {
    return (
      <div className="search-bar">
        <div className="search-container">
          <SearchInput changeHandler={this.changeHandler} />
          <SearchButton clickHandler={this.clickHandler} loading={this.props.loading} disabled={!this.state.singer} />
        </div>
      </div>
    );
  }
}

export default connector(SearchBar);
