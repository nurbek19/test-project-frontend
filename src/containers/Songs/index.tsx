import React, {Fragment} from "react";
import {connect, ConnectedProps} from "react-redux";
import {ThunkDispatch} from "redux-thunk";
import {AnyAction} from "redux";

import SongCard from "../../components/SongCard";

import {RootState} from "../../store/configureStore";
import {fetchSongs} from "../../store/actions";

import './index.css';


export const convertToMinute = (seconds: number): string => {
  return ~~(seconds / 60) + ':' + (seconds % 60 < 10 ? '0' : '') + seconds % 60;
};

const mapStateToProps = (state: RootState) => ({
  loading: state.loading,
  songs: state.songs,
  error: state.error
});

const mapDispatchToProps = (dispatch: ThunkDispatch<RootState, void, AnyAction>) => ({
  fetchSongs: (singer: string, index?: string | undefined) => dispatch(fetchSongs(singer, index))
});

const connector = connect(
  mapStateToProps,
  mapDispatchToProps
);

type PropsFromRedux = ConnectedProps<typeof connector>

const Songs: React.FC<PropsFromRedux> = (props) => {
  const {data, next, prev} = props.songs;
  let nextParams: URLSearchParams;
  let prevParams: URLSearchParams;

  if (props.loading) {
    return <p className="preloader">Загрузка...</p>;
  }

  if (props.error) {
    return <p className="preloader">Ничего не найдено :(</p>;
  }

  if (next) {
    nextParams = new URL(next).searchParams;
  }

  if (prev) {
    prevParams = new URL(prev).searchParams;
  }

  const songsDuration = data.reduce((sum, song) => {
    return sum + song.duration;
  }, 0);

  return (
    <div className="songs-container">
      {!!data.length && (
        <Fragment>
          <div className="songs-list">
            {data.map((song) => (
              <SongCard song={song} key={song.id} />
            ))}
          </div>

          <div className="songs-duration">
            <span>Общая длительность: {convertToMinute(songsDuration)}</span>
          </div>

          <div className="buttons">
            <div>
              {prev && (
                <button onClick={() => props.fetchSongs(prevParams.get('q') as string, prevParams.get('index') as string)}>Previous</button>
              )}
            </div>

            <div>
              {next && (
                <button onClick={() => props.fetchSongs(nextParams.get('q') as string, nextParams.get('index') as string)}>Next</button>
              )}
            </div>
          </div>
        </Fragment>
      )}
    </div>
  );
};

export default connector(Songs);
