import {Songs} from "./types";

export const FETCH_SONGS_REQUEST = 'FETCH_SONGS_REQUEST';
export const FETCH_SONGS_SUCCESS = 'FETCH_SONGS_SUCCESS';
export const FETCH_SONGS_FAILURE = 'FETCH_SONGS_FAILURE';

export interface FetchSongsRequest {
  type: typeof FETCH_SONGS_REQUEST
}

export interface FetchSongsSuccess {
  type: typeof FETCH_SONGS_SUCCESS,
  payload: Songs
}

export interface FetchSongsFailure {
  type: typeof FETCH_SONGS_FAILURE,
  payload: string | object
}

export type SongsActionTypes = FetchSongsRequest | FetchSongsSuccess | FetchSongsFailure;
