import axios from "axios";
import { ThunkAction } from "redux-thunk";
import { AnyAction } from "redux";

import {RootState} from "./configureStore";
import {Songs} from "./types";
import {
  FETCH_SONGS_FAILURE,
  FETCH_SONGS_REQUEST,
  FETCH_SONGS_SUCCESS,
  SongsActionTypes
} from "./actionTypes";



export const fetchSongsRequest = (): SongsActionTypes => ({
  type: FETCH_SONGS_REQUEST
});

export const fetchSongsSuccess = (songs: Songs): SongsActionTypes => ({
  type: FETCH_SONGS_SUCCESS,
  payload: songs
});

export const fetchSongsFailure = (error: string | object): SongsActionTypes => ({
  type: FETCH_SONGS_FAILURE,
  payload: error
});

export const fetchSongs = (singer: string, index?: string | undefined): ThunkAction<void, RootState, unknown, AnyAction> => (dispatch) => {
  dispatch(fetchSongsRequest());

  let query = `singer=${singer}`;

  if (index) {
    query = query + `&index=${index}`;
  }

  return (
    axios.get(`http://localhost:3000/songs?${query}`)
      .then(
        (response) => (dispatch(fetchSongsSuccess(response.data))),
        (error) => (dispatch(fetchSongsFailure(error.message)))
      )
  );
};


