export interface Song {
  id: number;
  name: string;
  link: string;
  albumCover: string;
  artist: string;
  duration: number;
}

export interface Songs {
  data: Song[],
  total: number;
  prev?: string;
  next?: string;
}

export interface SongsState {
  songs: Songs,
  loading: boolean,
  error: null | string | object
}
