import {SongsState} from "./types";
import {
  FETCH_SONGS_FAILURE,
  FETCH_SONGS_REQUEST,
  FETCH_SONGS_SUCCESS,
  SongsActionTypes
} from "./actionTypes";


const initialState: SongsState = {
  songs: {
    data: [],
    total: 0
  },
  loading: false,
  error: null
};

export const songsReducer = (state = initialState, action: SongsActionTypes): SongsState => {
  switch (action.type) {
    case FETCH_SONGS_REQUEST:
      return {...state, loading: true, error: null};
    case FETCH_SONGS_SUCCESS:
      return {...state, songs: action.payload, loading: false};
    case FETCH_SONGS_FAILURE:
      return {...state, loading: false, error: action.payload};
    default:
      return state;
  }
};
