import {applyMiddleware, compose, createStore} from "redux";
import thunkMiddleware from "redux-thunk";

import {songsReducer} from "./reducer";

// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type RootState = ReturnType<typeof songsReducer>

export default createStore(songsReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));
